using System.IO;
using System.Net;
using System.Threading.Tasks;
using Xunit;
using Amazon.Lambda.DynamoDBEvents;
using Amazon.Lambda.TestUtilities;
using Newtonsoft.Json;
using Shouldly;


namespace AwsLambda.Tests
{
    public class FunctionTest
    {
        private readonly Function _sut;
        private readonly TestLambdaContext _context;
        private readonly DynamoDBEvent _event;

        public FunctionTest()
        {
            _sut = new Function();
            _context = new TestLambdaContext();

            var json = File.ReadAllText("sample-event.json");
            _event = JsonConvert.DeserializeObject<DynamoDBEvent>(json);
        }

        [Fact]
        public async Task SendToFirehoseSuccess()
        {
            var response = await _sut.HandleDynamoUpdate(_event, _context);

            response.ShouldNotBeNull();
            response.HttpStatusCode.ShouldBe(HttpStatusCode.OK);
        }
    }
}
