using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Amazon.KinesisFirehose;
using Amazon.KinesisFirehose.Model;
using Amazon.Lambda.Core;
using Amazon.Lambda.DynamoDBEvents;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace AwsLambda
{
    public class Function
    {
        public async Task<PutRecordBatchResponse> HandleDynamoUpdate(DynamoDBEvent evnt, ILambdaContext context)
        {
            var firehoseClient = new AmazonKinesisFirehoseClient();
            var records = new List<Record>();

            var settings = BuildHost().Services.GetService<AppSettings>();

            foreach (var record in evnt.Records)
            {
                var json = JsonConvert.SerializeObject(record.Dynamodb);
                records.Add(new Record{
                    Data = new MemoryStream(Encoding.UTF8.GetBytes(json))
                });
            }

            var request = new PutRecordBatchRequest
            {
                DeliveryStreamName = settings.FirehoseStreamName,
                Records = records
            };

            var response = await firehoseClient.PutRecordBatchAsync(request);

            return response;
        }

        public IHost BuildHost()
        {
            var hostBuilder = new HostBuilder()
                .ConfigureAppConfiguration(builder =>
                {
                    builder.SetBasePath(Directory.GetCurrentDirectory());
                    builder.AddJsonFile("appsettings.json", optional: false);
                    builder.AddEnvironmentVariables();
                })
                .ConfigureServices((context, services) =>
                {
                    var settings = new AppSettings();
                    context.Configuration.Bind("AppSettings", settings);
                    services.AddSingleton(settings);
                });


            return hostBuilder.Build();
        }

        public class AppSettings
        {
            public string FirehoseStreamName { get; set; }
        }
    }
}